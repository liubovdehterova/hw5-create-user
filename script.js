/*
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Це дозволяє використовувати спецсимволи в рядках, щоб вони трактувалися як звичайний текст і не впливали його структуру
*/
/*
2. Які засоби оголошення функцій ви знаєте?
з ключовим словом "function":
function myFunction(param1, param2) {
  ...
}
Функції-вирази зі змінними:
const myFunction = function(param1, param2) {
   ...
};
Стрілкові функції:
const myFunction = (param1, param2) => {
  ...
};
як метод об'єкта:
const myObject = {
  myMethod: function(param1, param2) {
    ...
  }
};
*/
/*
3. Що таке hoisting, як він працює для змінних та функцій?
це процес, у якому інтерпретатор переміщує оголошення функцій,
змінних або класів у верхню частину їхньої області видимості перед тим як виконати код
*/

function createNewUser() {
    let _firstName = prompt("Введіть ім'я");
    let _lastName = prompt("Введіть прізвище");
    let _birthday = prompt("Введіть дату народження (текст у форматі dd.mm.yyyy)")

    let newUser = {
        get firstName() {
            return _firstName;
        },
        get lastName() {
            return _lastName;
        },
        get birthday() {
            return _birthday;
        },
        set firstName(newFirstName) {
            _firstName = newFirstName;
        },
        set lastName(newLastName) {
            _lastName = newLastName;
        },
        getLogin: function() {
            let result;
            //Перевіряємо чи введено ім'я
            if (this.firstName && this.firstName.length > 0) {
                // Створюємо логін з першої літери ім'я та всього прізвища
                result = this.firstName.charAt(0) + this.lastName;
            } else {
                throw new Error("Ім'я користувача не встановлено");
            }
            return result.toLowerCase(); //повертаємо все в нижньому регістрі
        },
        getAge: function () {
            let date = new Date(); //Поточна дата
            let dateDay = date.getDate(); // Поточний день місяця
            let dateMonth = date.getMonth(); // Поточний місяць
            let dateUser = this.birthday.split('.'); // Масив з дати, місяця та року народження користувача
            let age; //Зберігається вік

            // Додаємо 0 до поточного дня місяця якщо день - це не подвійне число
            if(date.getDate() < 10) {
                dateDay = '0' + date.getDate();
            }

            // Додаємо 0 до поточного місяця якщо місяць - це не подвійне число
            if(date.getMonth() < 10) {
                dateMonth = '0' + (date.getMonth()  + 1);
            }

            // Розрахунок віку
            age = date.getFullYear() - +dateUser[2];

            //Перевіряємо чи було день народження в цьому році
            if(+dateMonth < +dateUser[1]) {
                age--;
            } else if (dateDay < +dateUser[0]) {
                age--;
            }

            //Якщо людина вже народилась, але їй менше року і якщо дата народження введена наперед
            if (age === 0 && (dateMonth - +dateUser[1]) >= 1) {
                age = (dateMonth - +dateUser[1]) + ' місяць/місяця/місяці/місяців';
            } else if (age === 0 && (dateMonth - +dateUser[1]) === 0) {
                age = (dateDay - +dateUser[0]) + ' день/дні/дня/днів';
            } else if(age < 0 && (dateMonth - +dateUser[1]) < 1 && (dateDay - +dateUser[0]) < 1) {
                throw new Error("Не вірний вік");
            }

            return age;
        },
        getPassword: function () {
            let dateUser = this.birthday.split('.');
            let result = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + dateUser[2];
            return result;
        }
    };

    return newUser;
}

const user = createNewUser();

console.log("Ім'я користувача: ", user.firstName);
console.log("Прізвище користувача: ", user.lastName);
console.log("Логін: ", user.getLogin());
console.log("Генерація паролю, до оновлення ім'я та прізвища: ", user.getPassword());

user.firstName = "Pavel";
user.lastName = "Pavlov";

console.log("Змінене ім'я користувача: ", user.firstName);
console.log("Змінене прізвище користувача: ", user.lastName);
console.log("Логін: ", user.getLogin());


console.log("Дата народження користувача: ", user.birthday);
console.log("Вік: ", user.getAge());

console.log("Генерація паролю після оновлення ім'я та прізвища: ", user.getPassword());